# -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 16:14:35 2023

@author: poovi
"""
import os
from openpyxl import load_workbook
import pprint
from re import split,sub
'''
REGMAP = {'G': {'00': {'_name': 'PAGE',
              '_size': '8.0',
              '_print': 1,
              'PAGE[7:0]': ['None', 'None', '', 'Customer', 'h0', 'None']},
       '01': {'_name': 'OPERATION',
              '_size': '1.0',
              '_print': 1,
              'OPERATION[0]': ['None', 'None', '', 'Customer', 'h0', 'None']},   
              'OPERATION[1]': ['None', 'None', '', 'Customer', 'h0', 'None']}},                        
'''
def Reg2dict(Book,pyfile,var):
    wb = load_workbook(Book,data_only=True)
    sheet = wb["PMBus Registers"]
    reg_dict = {'G':{},'A':{},'B':{},'C':{}}#OrderedDict()
    pages = ['G','A','B','C']
    _print = 1
    col_dict = {}
    #Get column number by matching with the heading 
    for i in range(1,sheet.max_column):
        col_dict[sheet.cell(1,i).value] = i
    # print(col_dict)
    reg_col = col_dict['ADDRESS']
    default_col_A = col_dict['4663 Bringup Value RailA']
    default_col_B = col_dict['4663 Bringup Value RailB']
    default_col_C = col_dict['4663 Bringup Value RailC']
    paged_col = col_dict['PAGED']
    size_col = col_dict['Width']
    bitname_col = col_dict['BitName']
    end_col = col_dict['MSB']
    start_col = col_dict['LSB']
    read_col = col_dict['Readable']
    write_col = col_dict['Writable']
    descrip_col = col_dict['Description']
    name_col = col_dict['REG_NAME']
    Data_Str_F1_col = col_dict['Data_Str_F1']
    module_col = col_dict['Module']
    
    for p in pages:
        bit = 0
        invalid = []
        if p == 'C':
            default_col = default_col_C#U #RailC
        elif p == 'B':
            default_col = default_col_B#V #RailB
        else:
            default_col = default_col_A# W #RailA
        for i in range(1,sheet.max_row):
            #Get data from excel
            register = str(sheet.cell(i, reg_col).value) # E
            default = str(sheet.cell(i, default_col).value) 
            if i >= 2 and register!='None':
                if i == 605:
                    print(i)
                paged = int(sheet.cell(i, paged_col).value) # D
                if ((paged == 1 and p in ('A','B','C')) or (paged == 0 and p in ('G'))) and default != 'None':
                    size = sheet.cell(i, size_col).value
                    #binames are repeating for F3 and F4. To have unique key add Data_Str_F1 string 
                    if register in ('F3','F4'):
                        str_f1 = str(sheet.cell(i, Data_Str_F1_col).value)
                        bitname = f'{str(sheet.cell(i, bitname_col).value)}_{str_f1}'
                    else:
                        bitname = str(sheet.cell(i, bitname_col).value) # J

                    end = int(sheet.cell(i, end_col).value) # L
                    start = int(sheet.cell(i, start_col).value) # M
                    # readwrite = '' #TODO get from excel 
                    read = str(sheet.cell(i, read_col).value)
                    write = str(sheet.cell(i, write_col).value)
                    # read_write   = str(sheet.cell(i, 3).value) # R/W or R
                    if read == 1 and write == 0:
                        Access_type = 'R'
                    elif read == 0 and write == 1:
                        Access_type = 'W'
                    else:
                        Access_type = 'R/W'
                    description = str(sheet.cell(i, descrip_col).value) # P
                    name = str(sheet.cell(i, name_col).value) # H

                    # bitfield_Name = sub(r"[\([{})\]:]", "_", name)[:-1]
                    
                    # Default value validation and converting from hex to bin 
                    if default == 'None' or 'h' not in default:
                        if register not in invalid:
                            invalid.append(register)
                    else:
                        bin_default = int(default[1:],16) #converting h0 to binary value

                    # check if the upcomming register is same 
                    # if the register is not the same update the dictionary values
                    # if the register is same add the bit size and add bitfieldname
                    if register not in reg_dict[p].keys():
                        reg_dict[p][register] = {}
                        reg_dict[p][register]['_name'] = name
                        reg_dict[p][register]['_size'] = size
                        reg_dict[p][register]['_print'] = _print
                        reg_dict[p][register][bitname] = [end,start,Access_type,'Customer',bin_default,description]
                    else:
                        # print(register,reg_dict[p][register]['_size'],size)
                        bit =  reg_dict[p][register]['_size']+size
                        reg_dict[p][register]['_size'] = bit
                        reg_dict[p][register][bitname]=[end,start,Access_type,'Customer',bin_default,description]
                    
                    #create seperate dictionary for au46xx with SVI3 for E8 and E9 register
                        
    if invalid !=[]:
        print(f'registers {invalid} have invalid default value')
    # print(reg_dict)
    #write to a .py file
    varible_name = "REGMAP"
    # if var == 'au46xx' and register in ('E8','E9'): and str(sheet.cell(i, module_col).value) == 'SVI3':
    reg_dic_46xx = {}
    reg_dic_45xx = {}
    # for k in reg_dict.keys():
    #     if var == 'au46xx':
            
            
    file = open(pyfile,'w',encoding="utf-8")
    file.write(varible_name+" = "+pprint.pformat(reg_dict,sort_dicts=False))
    file.close()    
                        
if __name__ == "__main__":
    # Read the excel from specified location
    cwd = os.getcwd()
    Book = (fr"{cwd}\chipconfig\backend\xtools\docs\46xx_PMBus_Regmap_Gen2.xlsx")
    pyfile_45xx = fr"{cwd}\chipconfig\backend\au45xx_pro_regmap_gen2.py"
    pyfile_46xx = fr"{cwd}\chipconfig\backend\au45xx_pro_regmap_gen2_46xx.py"
    Reg2dict(Book,pyfile_45xx,var = 'au45xx')
    Reg2dict(Book,pyfile_46xx, var = 'au46xx')

